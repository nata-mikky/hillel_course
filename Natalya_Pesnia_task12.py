print('\nРеализация через функцию \n')


def funct_operators(z):
    def funct_arguments(x, y):
        if z == '+':
            return x + y
        elif z == '-':
            return x - y
        elif z == '*':
            return x * y
        elif z == '/':
            return x / y
        elif z == '**':
            return x ** y
        else:
            return 'Error, такой операции нет'

    return funct_arguments


result_operations1 = funct_operators('+')
result_operations2 = funct_operators('-')
result_operations3 = funct_operators('*')
result_operations4 = funct_operators('/')
result_operations5 = funct_operators('**')
result_operations6 = funct_operators('&')

print(f'Результат суммирования 2-х чисел= {result_operations1(2, 3)}')
print(f'Результат вычитания 2-х чисел = {result_operations2(2, 3)}')
print(f'Результат умножение 2-х чисел = {result_operations3(2, 3)}')
print(f'Результат деления 2-х чисел = {result_operations4(2, 3)}')
print(f'Результат возведения в степень 2-х чисел = {result_operations5(2, 3)}')
print(f'Мат. операция, которой нет в списке = {result_operations6(2, 3)}')

print('\nРеализация с помощью lambda \n')


def funct_operators_l(z):
    funct_lambda = lambda x, y: x + y if z == '+' else (
        x - y if z == '-' else (x * y if z == '*' else (x / y if z == '/' else (x ** y if z == '**' else 'Error'))))
    return funct_lambda


res1 = funct_operators_l('+')
res2 = funct_operators_l('-')
res3 = funct_operators_l('*')
res4 = funct_operators_l('/')
res5 = funct_operators_l('**')
res6 = funct_operators_l('&')

print(f'Результат суммирования 2-х чисел = {res1(2, 3)}')
print(f'Результат вычитания 2-х чисел = {res2(2, 3)}')
print(f'Результат умножение 2-х чисел = {res3(2, 3)}')
print(f'Результат деления 2-х чисел = {res4(2, 3)}')
print(f'Результат возведения в степень 2-х чисел = {res5(2, 3)}')
print(f'Мат. операция, которой нет в списке = {res6(2, 3)}')
