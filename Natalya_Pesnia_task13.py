def decor(func):
    cache = {}

    def wrapper(*args):
        if args in cache:
            return cache[args]
        else:
            response = func(*args)
            cache[args] = response
            return response

    return wrapper