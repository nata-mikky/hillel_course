class Cat:
    def __init__(self, weight, length):
        self.weight = weight
        self.length = length

    def __add__(self, other):
        return CatDog(weight=(self.weight + other.weight) / 2, length=(self.length + other.length) / 2)


class Dog:
    def __init__(self, weight, length):
        self.weight = weight
        self.length = length

    def __add__(self, other):
        return CatDog(weight=(self.weight + other.weight) / 2, length=(self.length + other.length) / 2)


class CatDog:
    def __init__(self, weight, length):
        self.weight = weight
        self.length = length

    def __str__(self) -> str:
        return f"Вес Котопса: {self.weight} кг.\nДлинна котопса: {self.length} см"


cat1 = Cat(14, 66)
dog1 = Dog(78, 18)
cat_dog1 = cat1 + dog1
print(cat_dog1)

cat2 = Cat(1.7, 3.7)
dog2 = Dog(7.8, 0.7)
cat_dog2 = dog2 + cat2
print(cat_dog2)
