import math
import re
from itertools import zip_longest


def square_exp(sq: int) -> tuple:
    return 4 * sq, math.sqrt(2) * sq, sq ** 2


result = square_exp(int(input('Введите сторону квадрата: ')))
print('Результат: ', result)


def month_season(mth: int) -> str:
    if mth == 12 or mth == 1 or mth == 2:
        return 'Зима'
    elif mth == 3 or mth == 4 or mth == 5:
        return 'Весна'
    elif mth == 6 or mth == 7 or mth == 8:
        return 'Лето'
    elif mth == 9 or mth == 10 or mth == 11:
        return 'Осень'
    else:
        return "Ошибка, такого месяца нет"


result = month_season(int(input('Введите номер месяца: ')))

print(result)


def polindrom(text: str) -> bool:
    text = text.lower()
    text = text.replace(' ', '')
    text = re.sub("[,|.|:|;]", "", text)
    if text == text[::-1]:
        return True
    else:
        return False


result = polindrom(input('Введите текст: '))
print('Является ли строка полиндромом? -', result)


def funct_list(list1: tuple, list2: tuple) -> tuple:
    # new_list = [n_list for i in zip(list1, list2) for n_list in i] - если список одинаковой длинны
    new_list = [n_list for i in zip_longest(list1, list2, fillvalue=None) for n_list in i if n_list is not None]
    return str(new_list)


print('Новый список - ', funct_list([1, 2, 3, 4, 5, 6, 7, 8, 9], [10, 20, 30, 40, 50, 60, 70]))
print('Новый список - ', funct_list([1, 2, 3], [10, 20, 30, 40, 50, 60, 70]))


def distance_space(x2: int, y2: int, z2: int, x1: int = 0, y1: int = 0, z1: int = 0) -> float:
    delta_x = x2 - x1
    delta_y = y2 - y1
    delta_z = z2 - z1
    length = math.sqrt(delta_x ** 2 + delta_y ** 2 + delta_z ** 2)
    return length


print('Расстоняие между этими точкам = ', distance_space(4, 1, 2, 1, 1, 1))
print('Расстоняие между этими точкам = ', distance_space(2, 3, 4))
# print('Расстоняие между этими точкам = ', distance_space(2, 3, 4))

