mas = [['', '', '', '', ''],
       ['', '', '', '', ''],
       ['', '', '', '', ''],
       ['', '', '', '', ''],
       ['', '', '', '', '']]
r1 = '---'
row1 = ('*', '*', '*', '*', '*', '*')
st1 = r1.join(row1)
row2 = f'|\n{st1}\n|'
st2 = (row2.join(['|'.join(['{:^3}'.format(i) for i in r]) for r in mas]))
print(st1)
print(f'|{st2}|')
print(st1)

while True:
    print("""
    Выбери действие:
    1) Сделать запись
    2) Получить значение по координатам
    3) Показать все ячейки
    4) Удалить значение по координатам
    0) Программа завершает работу.
    """)

    user_inp = (input('Сделайте вывод от одного из действий от 0 до 4: '))
    if user_inp == '1':
        while True:
            x = int(input("Введите X: "))
            y = int(input("Введите Y: "))
            value = str(input("Введите value: "))
            if mas[x - 1][y - 1] == '':
                mas[x - 1][y - 1] = value
                print('Запись сделана!')
                break
            else:
                user_inp = (input('Ячейка занята! Перезаписать? \n 1) Да. \n 2) НЕТ! \n'))
                if user_inp == '2':
                    continue
                if user_inp == '1':
                    mas[x - 1][y - 1] = value
                    print("Запись сделана!")
                    break
    elif user_inp == '2':
        x = int(input("Введите X: "))
        y = int(input("Введите Y: "))
        if mas[x - 1][y - 1] == '':
            print('Ячейка пустая')
        else:
            print('Значение в ячейке = ', mas[x - 1][y - 1])
    elif user_inp == '3':
        r1 = '---'
        row1 = ('*', '*', '*', '*', '*', '*')
        st1 = r1.join(row1)
        row2 = f'|\n{st1}\n|'
        st2 = (row2.join(['|'.join(['{:^3}'.format(i) for i in r]) for r in mas]))
        print(st1)
        print(f'|{st2}|')
        print(st1)
        # print(mas)
    elif user_inp == '4':
        x = int(input("Введите X: "))
        y = int(input("Введите Y: "))
        mas[x - 1][y - 1] = ''
        print('Запись удалена!')
    elif user_inp == '0':
        break
