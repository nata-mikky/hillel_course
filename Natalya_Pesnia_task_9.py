print('Задача №1')
numbers = [77, 12, 2, 0, 0, 0, 1, 77, 12, 2, 7, 5, 12]
un_numbers = list(set(numbers))
print('Список уникальных чисел: ', un_numbers)

print('Задача №2')
list_numbers1 = [1, 7, 2, 3, 1, 19, 21, 11, 0, 222, 234]
list_numbers2 = [1, 77, 2, 34, 11, 1, 12, 11, 0]
list_un_numbers1 = set(list_numbers1)
list_un_numbers2 = set(list_numbers2)
list_un_numbers = len(list_un_numbers1 & list_un_numbers2)
print('К-ство уникальных чисел с двух списков равен: ', list_un_numbers)

print('Задача №3')
text = 'В единственной строке записан текст записан текст текст новый тест тест тест тест тест test тест В'
my_dict = {}
for key in text.split():
    if my_dict.get(key) is None:
        my_dict[key] = 1
        # print(key)
    else:
        my_dict[key] += 1
        # print(key)
for key, val in my_dict.items():
    print(f'{key} - встречается {val} раз(а)')

