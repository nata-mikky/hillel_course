friends = ["John", "Marta", "James", "Amanda", "Marianna"]
print("Names".center(20, "*"))
for names in friends:
    print(f"{names}".rjust(20, " "))