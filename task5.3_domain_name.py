import re

text1 = "http://example.com/"
text2 = "https://example.com/"
text3 = "https://example09898_test.com.ua/"
text4 = "example.com"
text5 = "кремль.рф"

pattern = "^(http[s]?:\/\/)((\w-+)\.)?(([\w]+)(\.[\w-]+))\/?"
print(
    re.search(pattern, text1)
)
print(
    re.search(pattern, text2)
)
print(
    re.search(pattern, text3)
)
print(
    re.search(pattern, text4)
)
print(
    re.search(pattern, text5)
)