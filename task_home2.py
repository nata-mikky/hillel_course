# task_1
vegetarian_people = ["Marta", "Alex", "Mark"]
omnivorous_people = ["Anna", "John", "Monica", "Adam"]
vegetarian_people.extend(omnivorous_people)
print('vegetarian_people = ', vegetarian_people)
# task_2
debtor_list1 = {"Marta", "Alex", "Mark", "Anna", "Adam", "Nata"}
debtor_list2 = {"Anna", "John", "Monica", "Nata", "Anna", "Adam", "Pol", "Jack"}
debtor_list1.intersection(debtor_list2)
repeat_names = debtor_list1.intersection(debtor_list2)
print('repeat_names = ', list(set(repeat_names)))
# task_3
john = {
    "name" : "John",
    "street" : "Backer street 32",
    "age" : 32,
    "coordinates" : (55.742793, 37.615401),
    "children" : ['Mark', 'Marta']
}
print(john)



